#include <stdio.h>
#include <malloc.h>
#include <glib.h>
#include "global_structures.h"
#include "files/files_manager.h"
#include "ui/ui.h"
#include "network/udp_actions.h"
#include "command_line/command_interpreter.h"


int main(int argc, char *argv[])
{
    if (argc == 1)
    {
        printf("Please, specify a working directory as an argument of the program\n");
    }
    else if (argc == 2)
    {
        struct files_list_wrapper *files_list_wrapper = calloc(1, sizeof(struct files_list_wrapper));
        files_list_wrapper->files_list = NULL;

        if (get_shared_files(argv[1], files_list_wrapper))
        {
            printf("Could not find or process specified directory\n");
            return 1;
        }

        struct global_structure *global_structure = calloc(1, sizeof(struct global_structure));
        global_structure->root_path = argv[1];
        global_structure->files_list = files_list_wrapper->files_list;
        global_structure->close_program = 0;
        free(files_list_wrapper);

        draw_ui(global_structure);

        pthread_t *udp_listener_thread = malloc(sizeof(pthread_t));
        pthread_create(udp_listener_thread, NULL, (void *) create_udp_listener, global_structure);

        draw_header_windows(global_structure->windows_state->windows->header_window1, 
            global_structure->windows_state->windows->header_window2);
        draw_downloading_window(global_structure->windows_state);
        draw_uploading_window(global_structure->windows_state);
        draw_log_window(global_structure->windows_state);

        run_command_line(global_structure);

        pthread_cancel(*udp_listener_thread);
        pthread_join(*udp_listener_thread, NULL);

        free_global_structure(global_structure);

        free(udp_listener_thread);
    }
    else
    {
        printf("To many arguments\n");
    }

    return 0;
}
