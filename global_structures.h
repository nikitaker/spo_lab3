#include <glib.h>
#include <curses.h>
#pragma once


struct windows
{
    WINDOW *header_window1;
    WINDOW *header_window2;
    WINDOW *downloading_window;
    WINDOW *uploading_window;
    WINDOW *log_window;
    WINDOW *command_line_window;
};


struct windows_state
{
    struct windows *windows;
    GList *logs;
    GList *uploading_files;
    GList *downloading_files;
    pthread_mutex_t logs_mutex;
    pthread_mutex_t uploading_mutex;
    pthread_mutex_t downloading_mutex;
};


struct global_structure
{
    char *root_path;
    struct windows_state *windows_state;
    GList *files_list;
    char close_program;
};


void free_global_structure(struct global_structure *global_structure);
