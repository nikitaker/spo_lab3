#include <glib.h>
#pragma once


struct files_list_wrapper
{
    GList *files_list;
};


struct file
{
    char *name;
    int size;
    char *md5_hash;
    char *relative_path;
};

int get_file_size(char *file_path);

int get_shared_files(char *working_dir_path, struct files_list_wrapper *files_list_wrapper);

void free_file(struct file *file);

struct file *description_to_file(char *file_description);
