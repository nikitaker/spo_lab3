#include "ui.h"
#include <curses.h>
#include <pthread.h>
#include "../utils/utils.h"
#include "../utils/consts.c"
#include "../files/files_manager.h"


struct windows *create_windows()
{
    struct windows *windows = calloc(1, sizeof(struct windows));

    int h, w;
    getmaxyx(stdscr, h, w);

    windows->header_window1 = newwin(3, w / 2, 0, 0);
    windows->header_window2 = newwin(3, w - w / 2, 0, w / 2);
    windows->downloading_window = newwin(h / 2 - 4, w / 2, 3, 0);
    windows->uploading_window = newwin(h / 2 - 4, w - w / 2, 3, w / 2);
    windows->log_window = newwin((h - 1) - (h / 2 - 1), w, h / 2 - 1, 0);
    windows->command_line_window = newwin(1, w, h - 1, 0);

    return windows;
}


void draw_header_windows(WINDOW *header_window1, WINDOW *header_window2)
{
    int h, w;
    getmaxyx(header_window1, h, w);
    wclear(header_window1);
    box(header_window1, 0, 0);
    wmove(header_window1, 1, 1);
    wprintw(header_window1, "Downloading");
    wrefresh(header_window1);

    getmaxyx(header_window2, h, w);
    wclear(header_window2);
    box(header_window2, 0, 0);
    wmove(header_window2, 1, 1);
    wprintw(header_window2, "Uploading");
    wrefresh(header_window2);
}


void draw_downloading_window(struct windows_state *windows_state)
{
    WINDOW *downloading_window = windows_state->windows->downloading_window;

    int h, w;
    getmaxyx(downloading_window, h, w);
    wclear(downloading_window);
    box(downloading_window, 0, 0);

    int first_row = 1;
    int total_rows = h - first_row;
    GList *current_element = g_list_first(windows_state->downloading_files);

    for (int i = 0; i <= total_rows; i++)
    {
        if (current_element != NULL)
        {
            struct downloading_file *downloading_file = (struct downloading_file *) current_element->data;
            wmove(downloading_window, first_row + i, 1);
            wprintw(downloading_window, downloading_file->message);
            current_element = current_element->next;
        }
        else
        {
            break;
        }
    }

    wrefresh(downloading_window);
}


void draw_uploading_window(struct windows_state *windows_state)
{
    WINDOW *uploading_window = windows_state->windows->uploading_window;

    int h, w;
    getmaxyx(uploading_window, h, w);
    wclear(uploading_window);
    box(uploading_window, 0, 0);

    int first_row = 1;
    int total_rows = h - first_row;
    GList *current_element = g_list_first(windows_state->uploading_files);

    for (int i = 0; i <= total_rows; i++)
    {
        if (current_element != NULL)
        {
            struct uploading_file *uploading_file = (struct uploading_file *) current_element->data;
            wmove(uploading_window, first_row + i, 1);
            wprintw(uploading_window, uploading_file->message);
            current_element = current_element->next;
        }
        else
        {
            break;
        }
    }

    wrefresh(uploading_window);
}


void draw_log_window(struct windows_state *windows_state)
{
    WINDOW *log_window = windows_state->windows->log_window;

    int h, w;
    getmaxyx(log_window, h, w);
    wclear(log_window);
    box(log_window, 0, 0);
    wmove(log_window, 1, 1);
    wprintw(log_window, "Actions/events log");

    int first_row = 2;
    int total_rows = h - first_row - 2;
    GList *current_element = g_list_last(windows_state->logs);

    for (int i = total_rows; i >= first_row; i--)
    {
        if (current_element != NULL)
        {
            wmove(log_window, first_row + i, 1);
            wprintw(log_window, current_element->data);
            current_element = current_element->prev;
        }
        else
        {
            break;
        }
    }

    wrefresh(log_window);
}


void draw_command_line_window(WINDOW *command_line_window)
{
    wclear(command_line_window);
    wprintw(command_line_window, "> ");
    wrefresh(command_line_window);
}


void ui_print(struct windows_state *windows_state, char *string, char show_time)
{
    pthread_mutex_lock(&windows_state->logs_mutex);

    char *log_row = calloc(1, MAX_LOG_ROW_LENGTH);

    char *buffer = calloc(1, TIME_BUFFER_LENGTH);

    if (show_time)
    {
        time_t timer;
        struct tm* timer_info;
        timer = time(NULL);
        timer_info = localtime(&timer);
        strftime(buffer, TIME_BUFFER_LENGTH, "[%Y-%m-%d %H:%M:%S] ", timer_info);
    }
    else
    {
        memset(buffer, ' ', LOGS_PADDING);
    }
    strcat(log_row, buffer);
    free(buffer);

    strcat(log_row, string);
    windows_state->logs = g_list_append(windows_state->logs, log_row);

    draw_log_window(windows_state);
    draw_command_line_window(windows_state->windows->command_line_window);

    pthread_mutex_unlock(&windows_state->logs_mutex);
}


void add_uploading_file(struct windows_state *windows_state)
{
    pthread_mutex_lock(&windows_state->uploading_mutex);
    pthread_mutex_unlock(&windows_state->uploading_mutex);
}


void update_downloading_file(struct windows_state *windows_state, struct downloading_file *downloading_file)
{
    pthread_mutex_lock(&windows_state->downloading_mutex);

    GList *current_element = g_list_first(windows_state->downloading_files);

    char file_found = 0;

    while (current_element != NULL)
    {
        struct downloading_file *current_downloading_file = (struct downloading_file *) current_element->data;

        if (!strcmp(current_downloading_file->file_description, downloading_file->file_description))
        {
            file_found = 1;

            current_downloading_file->message = downloading_file->message;

            break;
        }

        current_element = current_element->next;
    }

    if (!file_found)
    {
        windows_state->downloading_files = g_list_append(windows_state->downloading_files, downloading_file);
    }

    draw_downloading_window(windows_state);
    draw_command_line_window(windows_state->windows->command_line_window);

    pthread_mutex_unlock(&windows_state->downloading_mutex);
}


void update_uploading_file(struct windows_state *windows_state, struct uploading_file *uploading_file)
{
    pthread_mutex_lock(&windows_state->uploading_mutex);

    GList *current_element = g_list_first(windows_state->uploading_files);

    char file_found = 0;

    while (current_element != NULL)
    {
        struct uploading_file *current_uploading_file = (struct uploading_file *) current_element->data;

        if (!strcmp(current_uploading_file->file_description, uploading_file->file_description))
        {
            file_found = 1;

            current_uploading_file->message = uploading_file->message;

            break;
        }

        current_element = current_element->next;
    }

    if (!file_found)
    {
        windows_state->uploading_files = g_list_append(windows_state->uploading_files, uploading_file);
    }

    draw_uploading_window(windows_state);
    draw_command_line_window(windows_state->windows->command_line_window);

    pthread_mutex_unlock(&windows_state->uploading_mutex);
}


void draw_ui(struct global_structure *global_structure)
{
    initscr();
    refresh();

    struct windows *windows = create_windows();

    struct windows_state *windows_state = calloc(1, sizeof(struct windows_state));
    windows_state->windows = windows;
    pthread_mutex_init(&windows_state->logs_mutex, NULL);
    pthread_mutex_init(&windows_state->uploading_mutex, NULL);
    pthread_mutex_init(&windows_state->downloading_mutex, NULL);

    draw_header_windows(windows->header_window1, windows->header_window2);
    draw_downloading_window(windows_state);
    draw_uploading_window(windows_state);
    draw_log_window(windows_state);
    draw_command_line_window(windows->command_line_window);

    global_structure->windows_state = windows_state;
}


void destroy_ui(struct windows_state *windows_state)
{
    delwin(windows_state->windows->header_window1);
    delwin(windows_state->windows->header_window2);
    delwin(windows_state->windows->downloading_window);
    delwin(windows_state->windows->uploading_window);
    delwin(windows_state->windows->log_window);
    delwin(windows_state->windows->command_line_window);

    free(windows_state->windows);

    GList *current_element = g_list_first(windows_state->logs);
    while(current_element != NULL)
    {
        free(current_element->data);
        current_element = current_element->next;
    }
    g_list_free(windows_state->logs);

    pthread_mutex_destroy(&windows_state->logs_mutex);
    pthread_mutex_destroy(&windows_state->uploading_mutex);
    pthread_mutex_destroy(&windows_state->downloading_mutex);

    free(windows_state);

    endwin();
}
